//get 路由实现
!function(mix){
	//获取处理后的path  去掉问好 和多余的/
	function pathfn(path){
		var a=path.split('?');
		if(a[1])path=a[0];
		path=path.split('//').join('/');
		return path;
	}
	//function interceptor
	function get1(fn,cb){
		return function(req,next){
			if(fn(pathfn(req.path)))cb(req,next);
			else next();
		}
	}
	//regExp interceptor
	function get2(reg,cb){
		return function(req,next){
			var para=reg.exec(pathfn(req.path));
			if(para){
				req.para=para;
				cb(req,next);
			}else next();
		}
	}
	//:xxx  xxx是可以扩展的
	function get3(ps,cb){
		var reg=get.type[ps[1]];
		return function(req,next){
			var path=pathfn(req.path);
			var para=path.substr(ps[0].length);
			if(pathfn(req.path).indexOf(ps[0])==0&&reg.test(para)){
				req.para=para;
				cb(req,next);
			}else next();
		}
	}
	//通配符  /xxxx*  *后面不允许有字符
	function get4(os,cb){
		return function(req,next){
			if(pathfn(req.path).indexOf(os[0])==0)cb(req,next);
			else next();
		}
	}
	//string interceptor
	function get5(path,cb){
		return function(req,next){
			if(path==pathfn(req.path))cb(req,next);
			else next();
		}
	}
	function get(path,cb){
		//匹配路径支持的5中情况
		if(TYPE.isFunction(path))this.use(get1(path,cb));
		if(TYPE.isRegExp(path))this.use(get2(path,cb));
		if(!TYPE.isString(path))return;
		var a=path.split(':');
		if(a.length!=1){
			this.use(get3(a,cb));
			return;
		}
		var b=path.split('*');
		if(b.length!=1){
			this.use(get4(b,cb));
			return;
		}
		this.use(get5(path,cb));
	}
	get.type={
		'number':/^\d+$/,
		'string':/^[^\/]+$/,
		'date':/^[0-9]{6,6}$/
	};
	mix({
		get:get
	});
}(route.mix);